package cool.domsgames.schematics.data;

import org.bukkit.block.data.BlockData;

public record LoadedSchematicBlock(LocationNoWorld location, BlockData blockData) {

    public LocationNoWorld getRelativeLocation() {
        return location;
    }

    public BlockData getBlockData() {
        return blockData;
    }
}
