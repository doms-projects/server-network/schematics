package cool.domsgames.schematics.json;

public class Schematic {
    public String name;
    public SchematicBlock[] blocks;
}
