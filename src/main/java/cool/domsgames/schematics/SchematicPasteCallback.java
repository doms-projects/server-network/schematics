package cool.domsgames.schematics;

import cool.domsgames.schematics.data.LocationNoWorld;
import org.bukkit.Location;
import org.bukkit.block.data.BlockData;

public interface SchematicPasteCallback {
    boolean blockPaste(String pasteId, BlockData block, Location centre, LocationNoWorld relativeLocation);
}
